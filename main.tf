# Configure the AWS provider
provider "aws" {
  region = "us-east-1" # Choose your desired region
}

# Create an IAM role for Lambda execution
resource "aws_iam_role" "lambda_execution_role" {
  name               = "lambda_execution_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect    = "Allow"
        Principal = {
          Service = "lambda.amazonaws.com"
        }
        Action    = "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_policy_attachment" {
  role       = aws_iam_role.lambda_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

# Create a Lambda function to stop EC2 instances
resource "aws_lambda_function" "stop_ec2_lambda" {
  filename         = "lambda_function.zip" # Path to your Lambda function code
  function_name    = "stopEC2Instances"
  handler          = "lambda_function.lambda_handler"
  source_code_hash = filebase64sha256("lambda_function.zip")
  runtime          = "python3.8"
  role             = aws_iam_role.lambda_execution_role.arn
}

# Invoke Lambda function
resource "aws_cloudwatch_event_rule" "schedule" {
  name                = "StopEC2InstancesSchedule"
  schedule_expression = "cron(0 18 ? * MON-FRI *)" # Schedule the Lambda function to run on weekdays at 18:00 UTC
}

resource "aws_cloudwatch_event_target" "target" {
  rule      = aws_cloudwatch_event_rule.schedule.name
  target_id = "StopEC2Instances"
  arn       = aws_lambda_function.stop_ec2_lambda.arn
}

# Create EC2 instances
resource "aws_instance" "ec2_instance" {
  count         = 10
  ami           = "ami-0f9c44e98edf38a2b" # Enter the appropriate Windows AMI ID
  instance_type = "t2.micro"
}
